Source: tup
Section: devel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 11),
 emacs-nox,
 libfuse-dev,
 libsqlite3-dev,
 pkg-config,
 procps,
 libpcre3-dev,
 libfuse3-dev,
Standards-Version: 4.2.1
Homepage: http://gittup.org/tup
Vcs-Browser: https://salsa.debian.org/debian/tup
Vcs-Git: https://salsa.debian.org/debian/tup.git

Package: tup
Architecture: linux-any
Depends:
 fuse,
 procps,
 ${misc:Depends},
 ${shlibs:Depends},
Description: fast build system
 Tup is a file-based build system for Linux, OSX, and Windows. It
 takes as input a list of file changes and a directed acyclic graph
 (DAG). It then processes the DAG to execute the appropriate commands
 required to update dependent files. Updates are performed with very
 little overhead since tup implements powerful build algorithms to
 avoid doing unnecessary work. This means you can stay focused on your
 project rather than on your build system.
