#!/usr/bin/make -f
export DEB_BUILD_MAINT_OPTIONS = hardening=+all qa=+canary
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk

%:
	dh $@
override_dh_auto_configure:
	echo 'CONFIG_TUP_USE_SYSTEM_SQLITE=y' > tup.config
	echo 'CONFIG_TUP_DEBUG=y' >> tup.config
	dpkg-buildflags | sed 's/=/+=/' > dpkgflags.tup

# build.sh script compiles tup in bootstrap mode, with pre-bundled
# sqlite3 and lua. We use it to rebuild tup again, properly this time
override_dh_auto_build: debian/tup.emacsen-startup
	rm -rf .pc
	./build.sh
	find -name '*.o' -delete
	find -name '*.a' -delete
	rm -f src/lua/lua                 \
	      src/luabuiltin/luabuiltin.h \
	      src/tup/version/version.c   \
	      tup_client.h                \
	      tup
	./build/tup init
	./build/tup generate --verbose build-nofuse.sh
	./build-nofuse.sh

# Generates autoload file for tupfile-mode. See more in
# dh_installemacsen(1).
debian/tup.emacsen-startup: debian/tupfile-mode.el
	emacs -Q --batch --eval \
	  '(update-file-autoloads "$(CURDIR)/$<" t "$(CURDIR)/$@")'
	# Portability note: assumes GNU sed, which accepts -i without argument.
	# But GNU sed is essential, on Debian, after all.
	sed -i '/^;/ d' $@
	sed -i '/^\s*$$/ d' $@
	sed -i '/^(provide/ d' $@
